<?php
class block_customcourses extends block_base {
	
	public function init() {
	
		/*$this->title = get_string('customblock', 'block_customblock');*/
		$this->title = 'Courses';
	}

	
	public function get_content() {
		
		if ($this->content !== null) {
			return $this->content;
		}

		$this->content         =  new stdClass;
		$this->content->text   = $this->get_courses();
		$this->content->footer = 'Footer here...';

		return $this->content;
	}

	private function get_courses() {
		$courses = get_courses();
		$s = '';
		
		if( is_array($courses) and count($courses) > 0 ) {
			$s = '<ul>';
			foreach( $courses as $c ) {
				
				$context = get_context_instance(CONTEXT_COURSE, $c->id);
				$enrolled = count_enrolled_users($context);
				
				$s .= '<li>';
				$s .= '<a href="/course/view.php?id='. $c->id .'">';
				$s .= $c->fullname .' ('. $enrolled .')';
				$s .= '</a>';
				$s .= '</li>';
				
			}
			
			$s .= '</ul>';
			
			
		}
		
		return $s;
		
	}
	
	private function get_enrolled_users() {
		
		
	}
}