<?php
$string['pluginname'] = 'Custom Courses Block Moodle Test';
$string['customcourses'] = 'Custom Courses';
$string['customcourses:addinstance'] = 'Add a new Custom Courses block';
$string['customcourses:myaddinstance'] = 'Add a new Custom Courses block to the My Moodle page';